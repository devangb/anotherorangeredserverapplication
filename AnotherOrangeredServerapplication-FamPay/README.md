# AnotherOrangeRedServerApplication - Fampay
- Application uses Python 3.7, PostgreSQL

- Install dependencies with pip using `pip install -r requirements.txt`

- Configure application settings in `mysite/setting.py`

- Run server with `python manage.py runserver 127.0.0.1:3000`

- Run background process to start youtube video fetch worker task wih `python manage.py process_tasks`

- Create superuser with

- Access admin interface at `127.0.0.1:3000/admin`

- API structure can be accessed at `127.0.0.1:3000/api`

- For search API `127.0.0.1:3000/api/videos/?query={searchterm}`


