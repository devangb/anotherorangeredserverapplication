from django.apps import AppConfig
from mysite.settings import YOUTUBE_FETCH_DELAY_SECONDS

class VideosConfig(AppConfig):
    name = 'videos'
    
    def ready(self):
      from .tasks import fetch_videos
      fetch_videos(repeat=YOUTUBE_FETCH_DELAY_SECONDS)
