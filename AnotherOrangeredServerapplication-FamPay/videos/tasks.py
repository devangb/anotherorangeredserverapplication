from background_task import background
from datetime import datetime
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from .models import Video, Thumbnail, LastFetchTime
from mysite.settings import YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, YOUTUBE_FETCH_DELAY_SECONDS, GOOGLE_DEVELOPER_API_KEYS
from dateutil import parser
from django.db import IntegrityError


@background(schedule=YOUTUBE_FETCH_DELAY_SECONDS)
def fetch_videos():
    query = 'cricket'#query_term
    last_fetch_time = LastFetchTime.objects.filter(search_word=query)
    if last_fetch_time.exists():
        last_fetch_time = last_fetch_time.first()
    else:
        last_fetch_time = LastFetchTime.objects.create(search_word=query)

    try:
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
            developerKey=GOOGLE_DEVELOPER_API_KEYS[0])

        # Call the search.list method to retrieve results matching the specified
        # query term.

        print("Published after : ---- " + last_fetch_time.fetch_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        search_response = youtube.search().list(
          q=query,
          part='id,snippet',
          type='video',
          order='date',
          publishedAfter=last_fetch_time.fetch_time.strftime('%Y-%m-%dT%H:%M:%SZ')
        ).execute()

    except HttpError as e:
        print(e)


    videos = []

    # Add each result to the appropriate list, and then display the lists of
    # matching videos, channels, and playlists.
    for search_result in search_response.get('items', []):
        try:
            if search_result['id']['kind'] == 'youtube#video':
                video = Video()
                video.video_id = search_result['id']['videoId']
                video.title = search_result['snippet']['title']
                video.description = search_result['snippet']['description']
                video.published_at = parser.isoparse(search_result['snippet']['publishedAt'])
                video.channel_id = search_result['snippet']['channelId']

                # Update last fetched time to last published time
                if last_fetch_time.fetch_time < video.published_at:
                    last_fetch_time.fetch_time = video.published_at

                video.thumbnails = []

                for key in search_result['snippet']['thumbnails'].keys():
                    result_thumbnail = search_result['snippet']['thumbnails'][key]
                    thumbnail = Thumbnail()
                    thumbnail.url = result_thumbnail['url']
                    thumbnail.type = key
                    thumbnail.width = result_thumbnail['width']
                    thumbnail.height = result_thumbnail['height']
                    video.thumbnails.append(thumbnail)

                video.thumbnails.save()
                video.save()
                print("saved")
        except IntegrityError as e:
            print(e)
            print("Saving failed for videoId: " + search_result['id']['videoId'])

        videos.append('%s (%s)' % (search_result['snippet']['title'],
                                   search_result['id']['videoId']))

    print(last_fetch_time.fetch_time)
    last_fetch_time.save()

    print('Videos:\n', '\n'.join(videos), '\n')




