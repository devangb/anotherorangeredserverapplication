from rest_framework import viewsets, authentication, mixins
from rest_framework.permissions import IsAuthenticated
from .serializers import VideoSerializer
from .models import Video
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector


class VideoViewSet(viewsets.ModelViewSet):
    serializer_class = VideoSerializer

    def get_queryset(self):
        query = self.request.query_params.get("query", None)
        if query:
            # Search Vectors with weights for search queries in title and description
            vector = SearchVector('title', weight='A') + SearchVector('description', weight='B')
            query = SearchQuery(query)
            queryset = Video.objects.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.1).order_by('rank')
        else:
            queryset = Video.objects.all()

        queryset.order_by('-published_at')

        return queryset
