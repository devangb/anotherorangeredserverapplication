from django.db import models
from datetime import datetime, timedelta

# Create your models here.

class Video(models.Model):
  video_id = models.CharField(max_length=128, unique=True)
  channel_id = models.CharField(max_length=128)
  title = models.CharField(max_length=512)
  description = models.TextField()
  published_at = models.DateTimeField()

  def __str__(self):
    return str(self.title)

  class Meta:
    indexes = [
      models.Index(fields=['title', 'description', ]),
      models.Index(fields=['-published_at', ]),
      models.Index(fields=['video_id'])
    ]
  


class Thumbnail(models.Model):
  video = models.ForeignKey(Video, on_delete=models.CASCADE, related_name='video_thumbnails')
  type = models.CharField(max_length=256)
  url = models.TextField()
  width = models.IntegerField()
  height = models.IntegerField()

  def __str__(self):
    return str(self.video.title)


#Last fetchtime for different keywords
class LastFetchTime(models.Model):
  search_word = models.CharField(max_length=256)
  fetch_time = models.DateTimeField(default=datetime.today() - timedelta(days=1))
